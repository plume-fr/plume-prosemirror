import React from "react";
export default ({update, id, updateBtn}) => <button onClick = { () => update(id)}>{updateBtn}</button>