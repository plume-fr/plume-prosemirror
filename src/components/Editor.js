import React from "react";
import { EditorState } from "prosemirror-state";
import { EditorView } from "prosemirror-view";
import { Schema, Node } from "prosemirror-model";
import { addListNodes } from "prosemirror-schema-list";
import { schema as basicSchema } from "prosemirror-schema-basic";
import { exampleSetup, buildMenuItems } from "prosemirror-example-setup";
import UploadContentButton from "./utils/upload";
import UpdateContentButton from  "./utils/update";

const editorSchema = new Schema({
  nodes: addListNodes(basicSchema.spec.nodes, "paragraph block*", "block"),
  marks: basicSchema.spec.marks
});
const menu = buildMenuItems(editorSchema);
const plugins = [
  ...exampleSetup({
    schema: editorSchema,
    menuContent: menu.fullMenu
  }),
]

class Editor extends React.Component {

  componentDidMount() {
    const { importedDoc, saveInterval} = this.props;

    if(saveInterval) {
      this.interval = setInterval(() => this.setState({ time: Date.now() }), saveInterval);
    }
    
    let doc = Node.fromJSON(editorSchema, {
      type: "doc",
      content: []
    });
    try {
      doc = Node.fromJSON(editorSchema, importedDoc)
    }
    catch(error) {
      console.error(error);
    }
    this.editorView = new EditorView(this.editorRef, {
      state: EditorState.create({
        schema: editorSchema,
        doc,
        plugins
      }),
      attributes: {
        spellcheck: "false"
      }
    });


  }

  componentWillUnmount() {
    const { saveInterval } = this.props;
    if(saveInterval) {
      clearInterval(this.interval);
    }
  }

  handleEditorRef = el => {
    this.editorRef = el;
  };



  render() {
    const {  id, elements, updateBtn, uploadBtn, updateContent, saveInterval } = this.props;

    const upload = (elements) => {
      const initDoc = {
        type: "doc",
        content: elements
      };
      const newState = EditorState.create({editorSchema, doc: Node.fromJSON(editorSchema, initDoc), plugins});
      this.editorView.updateState(newState);
   };

    const update = (id) => {
      if(updateContent) {
        updateContent({
          id ,
          content: {
            type: "doc",
            content:this.editorView.state.doc.content
          }
        })
      }
    };

    if(saveInterval && updateContent) {
      setTimeout(() => {
        updateContent({
          id ,
          content: {
            type: "doc",
            content:this.editorView.state.doc.content
          }
        })
      }, 0);
      
    }

    return (

        <div>
          <ul>
            {updateBtn && (<li><UpdateContentButton update={update} id={id} updateBtn={updateBtn}/></li>)}
            {uploadBtn && (<li><UploadContentButton upload={upload} elements={elements} uploadBtn={uploadBtn}/></li>)}
          </ul>
          <div  ref={this.handleEditorRef} />
        </div>
  
    );
  }
}

export default Editor;
