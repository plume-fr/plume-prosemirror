# plume-editor

> 

[![NPM](https://img.shields.io/npm/v/plume-editor.svg)](https://www.npmjs.com/package/plume-editor) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save @plume-fr/plume-editor
```

## Usage

```jsx
import React, { Component } from 'react'

import Editor from '@plume-fr/plume-editor';
import  '@plume-fr/plume-editor/EditorDefaultStyle.css';

const initDoc =[
  {
    type: "heading",
    attrs: {
      level: 1
    },
    content: [
      {
        type: "text",
        text: "welcome to prosemirror"
      }
    ]
  }
];
  
export default class App extends Component {
  updateContent = () => {console.log('call your API here')}
  render () {
    const DDOC = {
      type: "doc",
      content: [
        {
          type: "heading",
          attrs: {
            level: 1
          },
          content: [
            {
              type: "text",
              text: "titre 1"
            }
          ]
        },
        {
          type: "paragraph",
          content: [
            {
              type: "text",
              text: "paragraphe 1"
            }
          ]
        },
        
        {
          type: "paragraph",
          content: [
            {
              type: "text",
              text: "paragraphe 2"
            }
          ]
        }
      ]
    };
    return (
      <div>
        <Editor
          importedDoc={DDOC}
          updateContent={this.updateContent}
          id={1}
          updateBtn='save'
          uploadBtn='upload'
          elements={initDoc}
        />
      </div>
    )
  }
}

```

## License

MIT © [sawsenfattahi](https://github.com/sawsenfattahi)
